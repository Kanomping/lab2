// const isUserNameValid = function(username){
//     return true;
// };

module.exports = {
    isUserNameValid: function (username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        if (username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },
    isAgeValid: function (age) {
        if (isNaN(parseInt(age))) {
            return false;
        }
        if (age < 18 || age > 100) {
            return false;
        }
        return true;
    },
    isPasswordValid: function (password) {
        if (password.length < 8) return false;
        num = 0, number = 0;
        for (let n = 0; n < password.length; n++) {
            if (password == password.charAt(n).match(/[A-Z]/)) return true;
            if (password == password.toLowerCase()) return false;
            if (password.charAt(n).match(/[0-9]/)) {
                num++;
            }
            if (password.charAt(n).match(/[\!\@\#\$\%\^\&\*\(\)\_\+\|\~\=\`\{\}\[\]\:\"\;\'\<\>\?\,\.\-]/)) {
                number++;
            }
        }
        if (number < 1) return false;
        if (num < 3) return false;
        return true;
    },
    isDateValid: function (day, month, year) {
        if (day < 1 || day > 31) return false;
        if (month < 1 || month > 12) return false;
        if (year < 1970 || year > 2020) return false;      
        // switch(month){
        //     case 1,3,5,7,8,10,12:
        //         if(day < 1 || day > 31){
        //             return false;
        //         }
        //         break;

        //     case 4,6,9,11:
        //         if(day < 1 && day > 30){
        //             return false;
        //         }
        //         break;
                
        //     case 2:
        //         if(day < 1 || day > 29){
        //             return false;
        //         }
        //         break;
        // }

        if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 15 ){
            if(day > 31) return false;
        }else if(month == 4 || month == 6 || month == 9 || month == 11){
            if(day > 30) return false;

        }else if(month == 2){
            if(day > 29) return false;
        }
        if(year%400 != 0 && year%100 != 0){
            if(month == 2){
                if(day > 28) return false;
            }
        }
        return true;
    }
}