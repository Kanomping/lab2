const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module',()=>{
    context('Function isUserNameValid',()=>{
        it('Function prototype : boolean isUserNameValid(username: String)',()=>{
            expect(validate.isUserNameValid('kob')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', ()=>{
            expect(validate.isUserNameValid('tu')).to.be.false;
        });
        it('ทุกตัวต้องเป็นตัวเล็ก', ()=>{
            expect(validate.isUserNameValid('Kob')).to.be.false;
            expect(validate.isUserNameValid('koB')).to.be.false;
        });
        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', ()=>{
            expect(validate.isUserNameValid('kob123456789012')).to.be.true;
            expect(validate.isUserNameValid('kob1234567890123')).to.be.false;
        });
    });
    context('Function isAgeValid',()=>{
        it('Function prototype : boolean isAgeValid (age: String)', ()=>{
            expect(validate.isAgeValid('18')).to.be.true;
        });
        it('age ต้องเป็นข้อความที่เป็นตัวเลข',()=>{
            expect(validate.isAgeValid('a')).to.be.false;
        });
        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', ()=>{
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('18')).to.be.true;
            expect(validate.isAgeValid('100')).to.be.true;
            expect(validate.isAgeValid('101')).to.be.false;
        });
    });
    context('Function isPasswordValid',()=>{
        it('Function prototype : boolean isUserNameValid(password: String)',()=>{
           expect(validate.isPasswordValid('Knp#091041')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', ()=>{
            expect(validate.isPasswordValid('313326')).to.be.false;
        });
        it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', ()=>{
            expect(validate.isPasswordValid('Knp#091041')).to.be.true;
            expect(validate.isPasswordValid('knp#091041')).to.be.false;
        });
        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', ()=>{
            expect(validate.isPasswordValid('Knp#091041')).to.be.true;
            expect(validate.isPasswordValid('Knp#09aaaa')).to.be.false;
        });
        it('ต้องมีอักขระ พิเศษ !@#$%^&*()_+|~-=\`{}[]:"; <>?,./ อย่างน้อย 1 ตัว', ()=>{
            expect(validate.isPasswordValid('Knp#091041')).to.be.true;
            expect(validate.isPasswordValid('Knp09aaaa')).to.be.false;
        });
    });
    context('function isDateValid',()=>{
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)',()=>{
            expect(validate.isDateValid('1','10','1998')).to.be.true;
        });
        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', ()=>{
            expect(validate.isDateValid('1','9','2000')).to.be.true;
            expect(validate.isDateValid('0','9','2000')).to.be.false;
            expect(validate.isDateValid('35','9','2000')).to.be.false;
        });
        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน',()=>{
            expect(validate.isDateValid('1','9','2000')).to.be.true;
            expect(validate.isDateValid('1','0','2000')).to.be.false;
            expect(validate.isDateValid('1','13','2000')).to.be.false;
        });
        it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020',()=>{
            expect(validate.isDateValid('1','9','2000')).to.be.true;
            expect(validate.isDateValid('1','1','1960')).to.be.false;
            expect(validate.isDateValid('1','1','2030')).to.be.false;
        });
        it('เดือนแต่ละเดือนมีจำนวนวันต่างกัน', ()=>{
            expect(validate.isDateValid('1','1','2000')).to.be.true;
            expect(validate.isDateValid('0','1','2000')).to.be.false;
            expect(validate.isDateValid('32','1','2000')).to.be.false;
            
            expect(validate.isDateValid('1','2','2000')).to.be.true;
            expect(validate.isDateValid('0','2','2000')).to.be.false;
            expect(validate.isDateValid('30','2','2000')).to.be.false;
             
            expect(validate.isDateValid('1','3','2000')).to.be.true;
            expect(validate.isDateValid('0','3','2000')).to.be.false;
            expect(validate.isDateValid('32','3','2000')).to.be.false;
             
            expect(validate.isDateValid('1','4','2000')).to.be.true;
            expect(validate.isDateValid('0','4','2000')).to.be.false;
            expect(validate.isDateValid('31','4','2000')).to.be.false;
             
            expect(validate.isDateValid('1','5','2000')).to.be.true;
            expect(validate.isDateValid('0','5','2000')).to.be.false;
            expect(validate.isDateValid('32','5','2000')).to.be.false;
             
            expect(validate.isDateValid('1','6','2000')).to.be.true;
            expect(validate.isDateValid('0','6','2000')).to.be.false;
            expect(validate.isDateValid('31','6','2000')).to.be.false;
             
            expect(validate.isDateValid('1','7','2000')).to.be.true;
            expect(validate.isDateValid('0','7','2000')).to.be.false;
            expect(validate.isDateValid('32','7','2000')).to.be.false;
            
            expect(validate.isDateValid('1','8','2000')).to.be.true;
            expect(validate.isDateValid('0','8','2000')).to.be.false;
            expect(validate.isDateValid('32','8','2000')).to.be.false;

            expect(validate.isDateValid('1','9','2000')).to.be.true;
            expect(validate.isDateValid('0','9','2000')).to.be.false;
            expect(validate.isDateValid('31','9','2000')).to.be.false;

            expect(validate.isDateValid('1','10','2000')).to.be.true;
            expect(validate.isDateValid('0','10','2000')).to.be.false;
            expect(validate.isDateValid('32','10','2000')).to.be.false;

            expect(validate.isDateValid('1','11','2000')).to.be.true;
            expect(validate.isDateValid('0','11','2000')).to.be.false;
            expect(validate.isDateValid('31','11','2000')).to.be.false;

            expect(validate.isDateValid('1','12','2000')).to.be.true;
            expect(validate.isDateValid('0','12','2000')).to.be.false;
            expect(validate.isDateValid('32','12','2000')).to.be.false;
        });
        it('กรณีของปี อธิกสุรทิน',()=>{
            expect(validate.isDateValid('29','2','2000')).to.be.true;
            expect(validate.isDateValid('29','2','2001')).to.be.false;
            expect(validate.isDateValid('28','2','2001')).to.be.true
        });
    });
});
